# Web-Crawler

This project is a demo of how to use [Phalcon](https://www.phalcon.io) framework to build a crawler application.

This crawler shows:

- Number of pages crawled
- Number of a unique images
- Number of unique internal links
- Number of unique external links
- Avg page load
- Avg page load
- Avg word count
- Avg Title length
- Table to display each page crawled and it's status code

Author: Eduardo Luz (https://www.eduardo-luz.com)