#!/bin/sh

if [ -z $1 ];
then
    VERSION=latest
else
    VERSION=$1
fi


docker build -t eduluz1976/php-fpm:${VERSION}  . 
