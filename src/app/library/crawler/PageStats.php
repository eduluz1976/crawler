<?php
declare(strict_types=1);

namespace eduluz1976\Crawler;

class PageStats
{
    private $url;
    private $statusCode;
    
    private $title;
    private $load;
    private $wordsCount;
    

    public function __construct($url, $statusCode, $title, $load, $wordsCount)
    {
        $this->url = $url;
        $this->statusCode = $statusCode;

        $this->title = $title;
        $this->load = $load;
        $this->wordsCount = $wordsCount;
    }

    /**
     * Get the value of wordsCount
     */
    public function getWordsCount()
    {
        return $this->wordsCount;
    }

 

    /**
     * Get the value of load
     */
    public function getLoad()
    {
        return $this->load;
    }



    /**
     * Get the value of title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the value of url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the value of statusCode
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
}
