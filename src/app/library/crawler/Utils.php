<?php
declare(strict_types=1);

namespace eduluz1976\Crawler;

class Utils
{
    const REGEXPR_EXTRACT_BODY_CONTENTS = '/^[\s\S]*<body[^\>]*>([\s\S]*)<\/body>[\s\S]*$/m';

    public static function str_word_count_utf8($str)
    {
        return count(preg_split('~[^\p{L}\p{N}\']+~u', $str));
    }


    public static function getEntireBody($text)
    {
        $output = [];
        \preg_match(self::REGEXPR_EXTRACT_BODY_CONTENTS, $text, $output);

        if (!empty($output) && isset($output[1])) {
            return \trim($output[1]);
        }
        return false;
    }

    public static function countWordsFromHTML($text)
    {
        $response = 0;

        // $output = [];
        // \preg_match(self::REGEXPR_EXTRACT_BODY_CONTENTS, $text, $output);

        $body = static::getEntireBody($text);

        // if (!empty($output) && isset($output[1])) {
        if ($body) {
            // $body = \trim($output[1]);
            $bodyWithoutScripts = self::removeScriptsBlocks($body);
            // $response = static::str_word_count_utf8(\trim(\strip_tags($bodyWithoutScripts)));
            $response = \str_word_count(\trim(\strip_tags($bodyWithoutScripts)));
        }
        return $response;
    }


    public static function removeScriptsBlocks($text)
    {
        $response = "";

        $startScriptTag = \strpos($text, '<script');

        if ($startScriptTag === false) {
            return $text;
        }

        $len = $startScriptTag;
        $response = \substr($text, 0, $len);
        $finishScriptTag = \strpos($text, '</script>', $startScriptTag);
        $ptrFinishScriptTag = $finishScriptTag + 9;
        

        // echo "\n\n starting / startScriptTag=$startScriptTag / ptrFinishScriptTag=$ptrFinishScriptTag / len=$len / finishScriptTag=$finishScriptTag  / '$response'";

        $continue = $finishScriptTag !== false;

        while ($continue) {
            $startScriptTag = \strpos($text, '<script', $ptrFinishScriptTag);
            if ($startScriptTag === false) {
                // echo "\n ==== finish...? ptrFinishScriptTag=$ptrFinishScriptTag";
                $response .= \substr($text, $ptrFinishScriptTag);
                $continue = false;
                continue;
            }
            $finishScriptTag = \strpos($text, '</script>', $startScriptTag);
            $continue = ($finishScriptTag !== false);
    
            if ($continue) {
                $len = ($startScriptTag - $ptrFinishScriptTag);
                $piece = \substr($text, $ptrFinishScriptTag, $len);
                $response .= $piece;
                // echo "\n piece = $piece" ;
                // echo "\n\n continue / startScriptTag=$startScriptTag / ptrFinishScriptTag=$ptrFinishScriptTag / len=$len / finishScriptTag=$finishScriptTag / '$text' / '$response'";
                $ptrFinishScriptTag = $finishScriptTag + 9;
            } else {
                // echo "\n finishing : ptrFinishScriptTag=$ptrFinishScriptTag";
                $response .= \substr($text, $ptrFinishScriptTag);
            }
        }




        // $finishScriptTag = $startScriptTag;
        // $hasDone = false;

        // while (!$hasDone) {
        //     $len = abs($start-$i);
        //     $response .= \substr($text, $start, $len);

        //     $f = \strpos($text, '</script>', $i);
                
        //     if ($f === false) {
        //         $hasDone = true;
        //         break;
        //     }

        //     $i = $f + 8;

        //     echo "\n\n i=$i f=$f start=$start  len=$len  ";
        //     echo "\n i/len = " . substr($text, $start, $len);
        //     // echo "\n i/len = " . substr($text, $i, $len);

        //     $start = \strpos($text, '<script', $i);
        //     if ($start === false) {
        //         $hasDone = true;
        //         $response .= \substr($text, $i);
        //     }
        // }

        return $response;
    }


    /**
     * Undocumented function
     *
     * @param string $uri
     * @param string $baseUri
     * @param boolean $filterQuestionMark
     * @return string
     */
    public static function normalizeUri($uri, $baseUri='', $filterQuestionMark=true)
    {
        $response = $uri;

        if (substr($uri, 0, 1) === '/' && substr($baseUri, -1)==='/') {
            // only one is necessary... removing from one of them
            $baseUri = substr($baseUri, 0, -1);
        } elseif (substr($uri, 0, 1) !== '/' && substr($baseUri, -1)!=='/') {
            // at least one is necessary
            $baseUri .= '/';
        }

        if (strpos(\strtolower($uri), 'http') === false) {
            $response = $baseUri . $uri;
        }

        if ($filterQuestionMark && \strpos($response, '?') !== false) {
            $response = substr($response, 0, \strpos($response, '?'));
        }

        return $response;
    }

    /**
     * Undocumented function
     *
     * @param string $uri
     * @param string $uriToCompare
     * @return boolean
     */
    public static function equalUri($uri, $uriToCompare='')
    {
        if ($uri === $uriToCompare) {
            return true;
        }

        // the length of both strings are very close
        if (\abs(\strlen($uri) - \strlen($uriToCompare)) <= 1) {
            if (substr($uri, -1) === '/'  &&  substr($uriToCompare, -1) !== '/') {
                $uriToCompare .= '/';
            } elseif (substr($uri, -1) !== '/'  &&  substr($uriToCompare, -1) === '/') {
                $uri .= '/';
            }
        }

        return ($uri === $uriToCompare);
    }
}
