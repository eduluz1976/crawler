<?php
declare(strict_types=1);

namespace eduluz1976\Crawler;

class CrawlerStats
{
    private $lsImages = [];
    private $lsInternalLinks = [];
    private $lsExternalLinks = [];
    private $lsPages = [];

    /**
     * Get the value of lsImages
     */
    public function getLsImages()
    {
        return $this->lsImages;
    }

    /**
     * Get the value of lsInternalLinks
     */
    public function getLsInternalLinks()
    {
        return $this->lsInternalLinks;
    }

    /**
     * Get the value of lsExternalLinks
     */
    public function getLsExternalLinks()
    {
        return $this->lsExternalLinks;
    }

    /**
     * Get the value of lsPages
     */
    public function getLsPages()
    {
        return $this->lsPages;
    }

    /**
     * Verify if this image is present on internal list
     *
     * @param string $url
     * @return boolean
     */
    public function isThisImagePresent($url)
    {
        return isset($this->lsImages[$url]);
    }

    /**
     * Adds an URL as internal image list.
     *
     * @param string $url
     * @return void
     */
    public function addImage($url)
    {
        $this->lsImages[$url] = 1;
    }

    /**
     * Checks if this link is present
     *
     * @param string $url
     * @return boolean
     */
    public function isThisLinkPresent($url)
    {
        return isset($this->lsExternalLinks[$url]) || isset($this->lsInternalLinks[$url]) ;
    }

    /**
     * Adds an URL for the internal links list
     *
     * @param string $url
     * @return void
     */
    public function addInternalLink($url)
    {
        $this->lsInternalLinks[$url] = false;
    }

    /**
     * Adds an URL for the external links list
     *
     * @param string $url
     * @return void
     */
    public function addExternalLink($url)
    {
        $this->lsExternalLinks[$url] = false;
    }

    public function addPage($page)
    {
        $this->lsPages[$page->getUrl()] = $page;
    }

    public function getNextInternalLinkNotFetched()
    {
        foreach ($this->lsInternalLinks as $url => $wasFetched) {
            if (!$wasFetched) {
                $this->lsInternalLinks[$url] = true;
                return $url;
            }
        }
        return '';
    }
}
