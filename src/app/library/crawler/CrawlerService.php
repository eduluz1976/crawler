<?php
declare(strict_types=1);

namespace eduluz1976\Crawler;

use \GuzzleHttp\Client;

use Rct567\DomQuery\DomQuery;

class CrawlerService
{
    protected $currentJob;
    protected $httpClient;
    protected $countUrlsProcessed = 0;

    protected $baseURL = "";
    protected $currentURL = "";
    protected $stats;

    protected $dom;
    protected $currentPageInfo = [];
    protected $currentPpage;
    protected $totalPagesCrawled = 0;

    const INFO_URL = 'url';
    const INFO_HTTP_RESULTS = 'http_results';
    const INFO_HTML = 'html';
    const INFO_PAGE_TITLE = 'page_title';
    const INFO_WORD_COUNT = 'word_count';
    const INFO_HTTP_STATUS_CODE = 'status_code';


    public function __construct()
    {
        $this->stats = new CrawlerStats();
    }


    protected function setJobStatusRunning()
    {
        $this->getCurrentJob()->status = \Jobs::STATUS_RUNNING;
        $this->getCurrentJob()->modified_at = date('Y-m-d H:i:s');
        
        $this->getCurrentJob()->save();
    }


    /**
     * What is missing
     *
     * @return void
     */
    protected function crawl()
    {
        if ($this->countUrlsProcessed < $this->getCurrentJob()->max_pages) {
            try {
                $this->currentPageInfo[self::INFO_HTTP_RESULTS] = $this->getHttpClient()->request('GET', $this->currentPageInfo[self::INFO_URL]);
                $this->currentPageInfo[self::INFO_HTML] = $this->currentPageInfo[self::INFO_HTTP_RESULTS]->getBody()->getContents();

                $this->dom = new DomQuery($this->currentPageInfo[self::INFO_HTML]);

                $this->currentPageInfo[self::INFO_PAGE_TITLE] =  $this->dom->find('title')->textContent;

    
                $body = $this->dom->find('body')->children()->contents();
                $text = $this->currentPageInfo[self::INFO_HTML];

    
            
                $this->currentPageInfo[self::INFO_WORD_COUNT] =  Utils::countWordsFromHTML($text);

                $this->currentPageInfo[self::INFO_HTTP_STATUS_CODE] = $this->currentPageInfo[self::INFO_HTTP_RESULTS]->getStatusCode();

                $ls = $body->find('img');

                foreach ($ls as $item) {
                    if ($item->attr('data-src')) {
                        $src = $item->attr('data-src');
                    } else {
                        $src =  $item->attr('src');
                    }
                

                    $url = Utils::normalizeUri($src, $this->currentPageInfo[self::INFO_URL]);

                

                    if (!$this->getStats()->isThisImagePresent($url)) {
                        $this->getStats()->addImage($url);
                    }
                }

                $ls = $body->find('a');

                foreach ($ls as $item) {
                    $src =  $item->attr('href');

                    $url = Utils::normalizeUri($src, $this->currentPageInfo[self::INFO_URL]);

                

                    if (!$this->getStats()->isThisLinkPresent($url)) {
                        if (\strpos($url, $this->currentPageInfo[self::INFO_URL]) === false) {
                            $this->getStats()->addExternalLink($url);
                        } else {
                            $this->getStats()->addInternalLink($url);
                        }
                    }
                }

                $this->currentPpage = new PageStats(
                    $this->currentPageInfo[self::INFO_URL],
                    $this->currentPageInfo[self::INFO_HTTP_STATUS_CODE],
                    $this->currentPageInfo[self::INFO_PAGE_TITLE] ?? '',
                    strlen($this->currentPageInfo[self::INFO_HTML]),
                    $this->currentPageInfo[self::INFO_WORD_COUNT]
            );
      
                $this->getStats()->addPage($this->currentPpage);
    
                $this->saveCurrentPageStats();
    
    
                $this->totalPagesCrawled++;
            } catch (\Exception $ex) {
                echo "\n\n\nERROR = ".$ex->getMessage() . "\n Code = " . $ex->getCode()."\n";
                $this->currentPageInfo[self::INFO_HTTP_STATUS_CODE] = $ex->getCode();
                $this->currentPageInfo[self::INFO_PAGE_TITLE]='';
                $this->currentPageInfo[self::INFO_HTML] = '';
                $this->currentPageInfo[self::INFO_WORD_COUNT]=0;
            }


            $this->countUrlsProcessed++;

            $url = $this->getStats()->getNextInternalLinkNotFetched() ?? '';
            while (Utils::equalUri($url, $this->currentPageInfo[self::INFO_URL])) {
                $url = $this->getStats()->getNextInternalLinkNotFetched();
            }
            if ($url) {
                $this->currentPageInfo[self::INFO_URL] = $url;
                $this->crawl();
            }
        }
    }

    protected function saveCurrentPageStats()
    {
        $page = new \Pages();
        $page->job_id = $this->currentJob->id;
        $page->url = $this->currentPpage->getUrl();
        $page->status = $this->currentPpage->getStatusCode();
        $page->title_length = strlen($this->currentPpage->getTitle());
        $page->page_load = $this->currentPpage->getLoad();
        $page->word_count = $this->currentPpage->getWordsCount();

        $page->save();
    }

    protected function saveFinalJobStats()
    {
        $result = new \Results();
        $result->job_id =  $this->currentJob->id;
        $result->num_pages_crawled = $this->totalPagesCrawled;
        $result->num_unique_images = count($this->getStats()->getLsImages());
        $result->num_unique_internal_links = count($this->getStats()->getLsInternalLinks());
        $result->num_unique_external_links = count($this->getStats()->getLsExternalLinks());


        $sum_page_load = 0;
        $sum_word_count = 0;
        $sum_title_length = 0;

        $countPages = 0;
        foreach ($this->getStats()->getLsPages() as $page) {
            $sum_page_load += $page->getLoad();
            $sum_word_count += $page->getWordsCount();
            $sum_title_length += strlen($this->currentPpage->getTitle());
            $countPages++;
        }

        if ($countPages>0) {
            $result->avg_page_load = $sum_page_load/ $countPages;
            $result->avg_word_count = $sum_word_count/ $countPages;
            $result->avg_title_length = $sum_title_length/ $countPages;
        } else {
            $result->avg_page_load = 0;
            $result->avg_word_count = 0;
            $result->avg_title_length = 0;
        }

        $result->save();
    }

    public function start($job)
    {
        $this->setCurrentJob($job);
        $this->setJobStatusRunning();

        // Set the initial URL
        $this->currentPageInfo[self::INFO_URL] = Utils::normalizeUri($this->getCurrentJob()->url);

        $this->crawl();


        $this->saveFinalJobStats();

        $job->status = \Jobs::STATUS_DONE;
        $job->save();

        return $this->getStats();
    }

    /**
     * Get the value of currentJob
     */
    public function getCurrentJob()
    {
        return $this->currentJob;
    }

    /**
     * Set the value of currentJob
     *
     * @return  self
     */
    public function setCurrentJob($currentJob)
    {
        $this->currentJob = $currentJob;

        return $this;
    }

    /**
     * Get the value of httpClient
     */
    public function getHttpClient()
    {
        if (!$this->httpClient) {
            $this->httpClient = new Client([
                'allow_redirects'=>[
                    'track_redirects' => true
                ]
                ]);
        }
        
        return $this->httpClient;
    }

    /**
     * Get the value of stats
     */
    public function getStats() : CrawlerStats
    {
        return $this->stats;
    }
}
