<h1>Jobs</h1>

{{ flash.output() }}

<div class="row">
    <div class="col-md-12">
    {% if isAuthenticated %}
        <a class="btn btn-success btn-sm float-right" href="/Jobs/new">
            <i class="fas fa-plus"></i> &nbsp; New Job
        </a>
        {% endif %}
    </div>
</div>

<div class="row">
    <div class="col-md-12">

{% if lsJobs.count() == 0  %}
    No Jobs found
{% else %}
<table class="table table-striped">
    <thead>
        <th>Title</th>
        <th>URL</th>
        <th>Status</th>
    </thead>
{% for job in lsJobs %}
<tr>
    <td> 
        <a href="/Jobs/view/{{ job.id }}">
        {{ job.title }} 
        </a>
    </td>
    <td> {{ job.url }} </td>
    <td> 
    
     <span
                    class="badge badge-pill 
                    {% switch job.status %}
                        {% case 9 %}
                            badge-danger 
                        {% case 3 %}
                            badge-success 
                        {% case 2 %}
                            badge-primary 
                        {% break %}
                        {% default %}
                            badge-secondary 
                    {% endswitch %}
                    "

                    >
                        {{ job.getStatusName() }}
                    </span>
    
    </td>
</tr>
{% endfor %}
</table>
    </div>
</div>

{% endif %}