<h1>New Job</h1>

<div class="row">
    <div class="col-md-12">
        <form method="POST" action="/Jobs/new">
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" class="form-control" placeholder="Job title" id="title" name="title">
            </div>
            <div class="form-group">
                <label for="url">URL:</label>
                <input type="url" class="form-control" placeholder="https://..." id="url" name="url">
            </div>
            <div class="form-group">
                <label for="max_pages">Max pages:</label>
                <input type="number" class="form-control" placeholder="5" value="5" id="max_pages" name="max_pages">
            </div>

            <div class="float-right"> 
                <a class="btn btn-link" href="/Jobs">Cancel</a>
                <button type="submit" class="btn btn-primary ">Create</button>
            </div>
        </form>

    </div>
</div>