

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-link btn-sm float-left" href="/Jobs">
            <i class="fas fa-arrow-left"></i> &nbsp; Return to Jobs list
        </a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h1>View job # {{ id }}</h1>
        <table class="table">
            <tr>
                <th>Title</th>
                <td>{{ job.title }}</td>
            </tr>
            <tr>
                <th>URL</th>
                <td>{{ job.url }}</td>
            </tr>
            <tr>
                <th>Max Pages to crawl</th>
                <td>{{ job.max_pages }}</td>
            </tr>
            <tr>
                <th>Status</th>
                <td >
                    <span
                    class="badge badge-pill 
                    {% switch job.status %}
                        {% case 9 %}
                            badge-danger 
                        {% case 3 %}
                            badge-success 
                        {% case 2 %}
                            badge-primary 
                        {% case 1 %}
                            badge-secondary 
                        {% break %}
                    {% endswitch %}
                    "

                    >
                        {{ job.getStatusName() }}
                    </span>

            

                    
                </td>
            </tr>
        </table>
    </div>    
</div>    

{% if results is defined  %}

<h1>Results</h1>
<div class="row">
    <div class="col-md-6">
        <h2>Stats</h2>
        <table class="table table-responsive">
            <tr>
                <th>Number of pages crawled</th>
                <td>{{ results.num_pages_crawled }}</td>
            </tr>
            <tr>
                <th>Number of a unique images</th>
                <td>{{ results.num_unique_images }}</td>
            </tr>
            <tr>
                <th>Number of unique internal links</th>
                <td>{{ results.num_unique_internal_links }}</td>
            </tr>
            <tr>
                <th>Number of unique external links</th>
                <td>{{ results.num_unique_external_links }}</td>
            </tr>
            <tr>
                <th>Avg page load</th>
                <td>{{ results.avg_page_load }}</td>
            </tr>
            <tr>
                <th>Avg word count</th>
                <td>{{ results.avg_word_count }}</td>
            </tr>
            <tr>
                <th>Avg Title length</th>
                <td>{{ results.avg_title_length }}</td>
            </tr>
        </table>
    </div>    
    <div class="col-md-6">
        <h2>Pages</h2>

        {% if results.getPages().count() == 0 %}

            No records found
        {% else %}

        <table class="table">
            <thead>
                <th>URL</th>
                <th>Status code</th>
            </thead>
            {% for page in results.getPages() %}
                <tr>
                    <td> {{ page.url }} </td>
                    <td> {{ page.status }} </td>
                </tr>
            {% endfor %}
        </table>

        {% endif %}
    </div>    

    {% endif %}
</div>    
