<h1>Simple Authentication</h1>

<div class="row">
    <div class="col-md-12">

        {{ flash.output() }}
        
        <form method="POST" action="/SimpleAuth/authenticate">
            <div class="form-group">
                <label for="token">Auth Token:</label>
                <input type="password" class="form-control" placeholder="..." id="token" name="token">
            </div>

            <div class="float-right"> 
                <a class="btn btn-link" href="/">Cancel</a>
                <button type="submit" class="btn btn-primary ">Proceed</button>
            </div>
        </form>

    </div>
</div>


    