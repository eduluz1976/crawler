
 
  {{ flash.output() }}
  
    <div class="row">       
             
        <div class="col-md-12">
                <h1>Web-Crawler</h1>


                <p>
                        This application is a demonstration of how to implement a web-crawler using <a href='https://phalcon.io/' target='_blank'>Phalcon</a> framework.
                </p>

                <p>

                This crawler shows:
                <ul>
                        <li>Number of pages crawled</li>
                        <li>Number of a unique images</li>
                        <li>Number of unique internal links</li>
                        <li>Number of unique external links</li>
                        <li>Avg page load</li>
                        <li>Avg page load</li>
                        <li>Avg word count</li>
                        <li>Avg Title length</li>
                        <li>Table to display each page crawled and it's status code</li>

                </ul>

                </p>

        </div>

        <div class="col-md-12">
                <h1 class='app-description'>Screens</h1>
        </div>

        <div class="offset-md-1  col-md-10">
                <h3 class='app-description'>List Jobs</h3>     
                <img src="/img/list-jobs.png" style="max-width: 100%;"/>
        </div>


        <div class="offset-md-1  col-md-10">
                <h3 class='app-description'>View a pending job</h3>     
                <img src="/img/view-pending-job.png" style="max-width: 100%;"/>
        </div>

        <div class="offset-md-1  col-md-10">
                <h3 class='app-description'>View a job done</h3>     
                <img src="/img/view-job-done.png" style="max-width: 100%;"/>
        </div>

    </div>

