<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Migrations\Mvc\Model\Migration;

/**
 * Class ResultsMigration_100
 */
class ResultsMigration_100 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('results', [
                'columns' => [
                    new Column(
                        'id',
                        [
                            'type' => Column::TYPE_BIGINTEGER,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 20,
                            'first' => true
                        ]
                    ),
                    new Column(
                        'job_id',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'id'
                        ]
                    ),
                    new Column(
                        'num_pages_crawled',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'job_id'
                        ]
                    ),
                    new Column(
                        'num_unique_images',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'num_pages_crawled'
                        ]
                    ),
                    new Column(
                        'num_unique_internal_links',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'num_unique_images'
                        ]
                    ),
                    new Column(
                        'num_unique_external_links',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'num_unique_internal_links'
                        ]
                    ),
                    new Column(
                        'avg_page_load',
                        [
                            'type' => Column::TYPE_DOUBLE,
                            'notNull' => true,
                            'after' => 'num_unique_external_links'
                        ]
                    ),
                    new Column(
                        'avg_word_count',
                        [
                            'type' => Column::TYPE_DOUBLE,
                            'notNull' => true,
                            'after' => 'avg_page_load'
                        ]
                    ),
                    new Column(
                        'avg_title_length',
                        [
                            'type' => Column::TYPE_DOUBLE,
                            'notNull' => true,
                            'after' => 'avg_word_count'
                        ]
                    )
                ],
                'indexes' => [
                    new Index('PRIMARY', ['id'], 'PRIMARY'),
                    new Index('job_id', ['job_id'], '')
                ],
                'references' => [
                    new Reference(
                        'results_ibfk_1',
                        [
                            'referencedSchema' => 'database',
                            'referencedTable' => 'jobs',
                            'columns' => ['job_id'],
                            'referencedColumns' => ['id'],
                            'onUpdate' => 'RESTRICT',
                            'onDelete' => 'RESTRICT'
                        ]
                    )
                ],
                'options' => [
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '17',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8mb4_general_ci'
                ],
            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
