<?php
declare(strict_types=1);

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    // Implement common logic

    public function initialize()
    {
        $this->view->isAuthenticated = $this->session->get('authorized');
    }
}
