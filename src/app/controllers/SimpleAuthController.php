<?php
declare(strict_types=1);

class SimpleAuthController extends ControllerBase
{
    public function indexAction()
    {
        //
    }

    public function logoutAction()
    {
        $this->session->set('authorized', 0);
        $this->flash->success("User logged out");
        $this->dispatcher->forward(
            [
                'controller' => 'Index',
                'action' => 'index'
            ]
        );
    }

    public function authenticateAction()
    {
        if (getenv('APP_PASSWORD') === $this->request->getPost('token')) {
            $this->session->set('authorized', 1);
            $this->flash->success("User authenticated");

            $this->dispatcher->forward(
                [
                    'controller' => 'Index',
                    'action' => 'index'
                ]
            );
        } else {
            $this->flash->error("Invalid auth token");
            $this->session->set('authorized', 0);

            $this->dispatcher->forward(
                    [
                        'controller' => 'SimpleAuth',
                        'action' => 'index'
                    ]
                );
        }
    }
}
