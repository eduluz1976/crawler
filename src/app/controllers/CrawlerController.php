<?php
declare(strict_types=1);

use \eduluz1976\Crawler\CrawlerService;

class CrawlerController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->message = "This is my test";
    }

    /**
     * Execute next job
     *
     * @return void
     */
    public function executeNext()
    {
        $response = [
            'success' => true
        ];

        $job = Jobs::findFirst(' status = 1');

        $response['job'] = json_encode($job);

        if ($job) {
            $service = new CrawlerService();
            $response['result'] = $service->start($job);
        }

        return $response;
    }

    public function resolveErrors()
    {
        $response = [
            
        ];

        $jobs = Jobs::find(' status = '. Jobs::STATUS_RUNNING);

        foreach ($jobs as $job) {
            $elapsed = time() - strtotime($job->modified_at);
            // 5 minutes of grace period
            if ($elapsed > 300) {
                $response[] = "\n Setting job " . $job->id . " as erroed by timeout ";
                $job->status = \Jobs::STATUS_ERROR;
                $job->save();
            }
        }

        return $response;
    }
}
