<?php
declare(strict_types=1);

class JobsController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->lsJobs = Jobs::find();
    }


    public function viewAction($id)
    {
        $job = Jobs::findFirstById($id);

        $this->view->job = $job;

        if ($job->isDone()) {
            $result = Results::findFirst("job_id = $id");

            if ($result) {
                $this->view->results = $result;
            }
        }

        $this->view->id = $id;
        // $this->view->msg = "Id = $id";
    }

    public function newAction()
    {
        if (!$this->session->get('authorized')) {
            $this->flash->error(
                "Operation not allowed - it is necessario to authenticate first"
            );
            $this->dispatcher->forward(
                [
                    'controller' => 'Jobs',
                    'action'     => 'index',
                ]
            );
        }

        if ($this->request->isPost()) {
            try {
                $job = new Jobs();
                $job->title =  $this->request->getPost('title');
                $job->url =  $this->request->getPost('url');
                $job->max_pages =  $this->request->getPost('max_pages');
                $job->save();
                $this->flash->success(
                    "Job created '".$job->title ."'"
                );
            } catch (\Exception $ex) {
                $this->flash->error(
                    "Error creating job '".$job->title ."' - " . $ex->getMessage()
                );
            }


            $this->dispatcher->forward(
                [
                    'controller' => 'Jobs',
                    'action'     => 'index',
                ]
            );
        }
    }
}
