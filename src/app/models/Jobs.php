<?php

class Jobs extends \Phalcon\Mvc\Model
{
    const STATUS_PENDING = 1;
    const STATUS_RUNNING = 2;
    const STATUS_DONE = 3;
    const STATUS_ERROR = 9;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $status = self::STATUS_PENDING;

    /**
     *
     * @var integer
     */
    public $max_pages;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $modified_at;

    /**
     *
     * @var string
     */
    public $url;

    /**
     *
     * @var string
     */
    public $title;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("database");
        $this->setSource("jobs");
        $this->hasMany('id', 'Pages', 'job_id', ['alias' => 'Pages']);
        $this->hasMany('id', 'Results', 'job_id', ['alias' => 'Results']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Jobs[]|Jobs|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Jobs|\Phalcon\Mvc\Model\ResultInterface|\Phalcon\Mvc\ModelInterface|null
     */
    public static function findFirst($parameters = null): ?\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($parameters);
    }


    public function getStatusName() : string
    {
        switch ($this->status) {
            case self::STATUS_PENDING: return 'Pending';
            case self::STATUS_RUNNING: return 'Running';
            case self::STATUS_DONE: return 'Done';
            case self::STATUS_ERROR: return 'Error';
            default: return 'Unknown';
        }
    }

    public function isDone()
    {
        return ($this->status == self::STATUS_DONE || $this->status == self::STATUS_ERROR);
    }
}
