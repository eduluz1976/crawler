<?php

class Results extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $job_id;

    /**
     *
     * @var integer
     */
    public $num_pages_crawled;

    /**
     *
     * @var integer
     */
    public $num_unique_images;

    /**
     *
     * @var integer
     */
    public $num_unique_internal_links;

    /**
     *
     * @var integer
     */
    public $num_unique_external_links;

    /**
     *
     * @var double
     */
    public $avg_page_load;

    /**
     *
     * @var double
     */
    public $avg_word_count;

    /**
     *
     * @var double
     */
    public $avg_title_length;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("database");
        $this->setSource("results");
        $this->belongsTo('job_id', '\Jobs', 'id', ['alias' => 'Jobs']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Results[]|Results|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Results|\Phalcon\Mvc\Model\ResultInterface|\Phalcon\Mvc\ModelInterface|null
     */
    public static function findFirst($parameters = null): ?\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($parameters);
    }

    public function getPages()
    {
        if (!isset($this->lsPages)) {
            $this->lsPages = Pages::find(" job_id = " . $this->job_id);
        }
        return $this->lsPages;
    }
}
