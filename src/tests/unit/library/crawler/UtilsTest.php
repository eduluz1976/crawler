<?php
use PHPUnit\Framework\TestCase;


use \eduluz1976\Crawler\Utils;

/**
 * UtilsTest
 */
class UtilsTest extends TestCase
{


    /**
     * @dataProvider supply_data_for_test_count_words
     * @test
     * */
    public function test_count_words($text, $expectedNumWords)
    {
        $numWords = Utils::countWordsFromHTML($text);
        $this->assertEquals($expectedNumWords, $numWords);
    }
    

    public function supply_data_for_test_count_words()
    {
        return [
            ['',0],
            ['This is not a valid HTML text, because there is no BODY tags',0],
            ['<html> Something <body> word1 word2 </body></html>', 2],
            ['<html> Something <body> word1 <script> something to ignore</script> word2 word3</body></html>', 3],
            ['<html> Something <body> word1 <script> something to ignore</script> word2 <!-- comments --> word3  word4 word5</body></html>', 5],
        ];
    }


    /**
     *
     * @dataProvider supply_data_for_test_removeScriptsBlocks
     * @param [type] $text
     * @param [type] $expectedResult
     * @return void
     */
    public function test_removeScriptsBlocks($text, $expectedResult)
    {
        $output = Utils::removeScriptsBlocks($text);
        $this->assertEquals($expectedResult, $output);
    }

    public function supply_data_for_test_removeScriptsBlocks()
    {
        return [
            [
                'Start <script> something to ignore </script> End',
                'Start  End'
            ],
            [
                "Start <script> something to ignore </script> Middle  <script> second block to ignore... </script> Finish",
                "Start  Middle   Finish"
            ],
            [
                "Start <script> something to ignore </script> Middle  <script>\n second block to ignore... </script>\n Finish",
                "Start  Middle  \n Finish"
            ],
            [
                "Start <script> Ignore until the end... ",
                "Start "
            ],
            [
                "<script> Ignore until the end... </script>",
                ""
            ],
            [
                "<script> Ignore some... </script>Show!",
                "Show!"
            ],
            [
                "<script> Some nested script <script> </script>Show!",
                "Show!"
            ],
        ];
    }

    /**
     * @test
     * @dataProvider supply_data_for_test_count_words_from_html
     * */
    public function test_count_words_from_html($filename, $expectedNumWords)
    {
        $text = file_get_contents(__DIR__.'/../../../data/'.$filename);

        $numWords = Utils::countWordsFromHTML($text);

        $this->assertEquals($expectedNumWords, $numWords);
    }

    public function supply_data_for_test_count_words_from_html()
    {
        return [
            ['file_0',665],
            ['file_1',829],
            ['file_2',722],
            ['file_3',636],
            ['file_4',640],
        ];
    }


    /**
     * @dataProvider supply_data_for_test_normalizeUri
     *
     * @return void
     */
    public function test_normalizeUri($baseUri, $uri, $expected)
    {
        $response = Utils::normalizeUri($uri, $baseUri);

        $this->assertEquals($expected, $response);
    }


    /**
     *
     *
     * @return void
     */
    public function supply_data_for_test_normalizeUri()
    {
        return [
            [
                'http://www.google.com/',
                "http://www.google.com/images/logo.png",
                "http://www.google.com/images/logo.png"
            ],
            [
                'http://www.google.com',
                "/images/logo.png",
                "http://www.google.com/images/logo.png"
            ],
            [
                'http://www.google.com/images',
                "/logo.png",
                "http://www.google.com/images/logo.png"
            ],
            [
                'http://www.google.com/',
                "images/logo.png",
                "http://www.google.com/images/logo.png"
            ],
            [
                'http://www.google.com',
                "images/logo.png",
                "http://www.google.com/images/logo.png"
            ],
            [
                'http://www.google.com/',
                "/images/logo.png",
                "http://www.google.com/images/logo.png"
            ],
            [
                'http://www.google.com/',
                "/images/logo.png",
                "http://www.google.com/images/logo.png"
            ],
            [
                '',
                "http://www.google.com",
                "http://www.google.com"
            ],
        ];
    }



    /**
     * @dataProvider supply_data_for_test_equalUri
     *
     * @return void
     */
    public function test_equalUri($uri, $uri2, $equals)
    {
        $response = Utils::equalUri($uri, $uri2);
        
        $this->assertEquals($equals, $response);
    }


    public function supply_data_for_test_equalUri()
    {
        return [
            [
                'http://www.google.com',
                'http://www.google.com/',
                true
            ],
            [
                'http://www.google.com/',
                'http://www.google.com/',
                true
            ],
            [
                'http://www.google.com/',
                'http://www.google.com',
                true
            ],
            [
                'http://www.facebook.com/',
                'http://www.google.com',
                false
            ],
            [
                'http://www.facebook.com/x',
                'http://www.facebook.com',
                false
            ],
            [
                'http://www.facebook.com/x',
                'http://www.facebook.com/y',
                false
            ],
            [
                'http://www.facebook.com/x',
                'http://www.facebook.com/x2',
                false
            ],
        ];
    }
}
