<?php

use PHPUnit\Framework\TestCase;

use \eduluz1976\Crawler\CrawlerStats;

/**
 * CrawlerStatsTest
 */
class CrawlerStatsTest extends TestCase
{
    /** @test */
    public function test_isThisImagePresent_when_image_does_not_exists_should_return_false()
    {
        $crawlerStats = new CrawlerStats();

        $image = "logo.png";

        $this->assertFalse($crawlerStats->isThisImagePresent($image));
    }

    /** @test */
    public function test_isThisImagePresent_when_image_does_exists_should_return_true()
    {
        $crawlerStats = new CrawlerStats();

        $image = "logo.png";
        $crawlerStats->addImage($image);

        $this->assertTrue($crawlerStats->isThisImagePresent($image));
    }

    /** @test */
    public function test_isThisLinkPresent_when_link_does_not_exists_should_return_false()
    {
        $crawlerStats = new CrawlerStats();

        $url = "/page";

        $this->assertFalse($crawlerStats->isThisLinkPresent($url));
    }

    /** @test */
    public function test_isThisLinkPresent_when_internal_link_does_exists_should_return_true()
    {
        $crawlerStats = new CrawlerStats();

        $url = "/page";
        $crawlerStats->addInternalLink($url);

        $this->assertTrue($crawlerStats->isThisLinkPresent($url));
        $this->assertEquals($url, array_keys($crawlerStats->getLsInternalLinks())[0]);
    }

    /** @test */
    public function test_isThisLinkPresent_when_external_link_does_exists_should_return_true()
    {
        $crawlerStats = new CrawlerStats();

        $url = "/page";
        $crawlerStats->addExternalLink($url);

        $this->assertTrue($crawlerStats->isThisLinkPresent($url));
        $this->assertEquals($url, array_keys($crawlerStats->getLsExternalLinks())[0]);
    }

    /** @test */
    public function test_getNextInternalLinkNotFetched_with_empty_list_should_return_false()
    {
        $crawlerStats = new CrawlerStats();

        $expected = false;

        $this->assertEquals($expected, $crawlerStats->getNextInternalLinkNotFetched());
    }

    /** @test */
    public function test_getNextInternalLinkNotFetched_with_a_list_should_return_a_valid_element()
    {
        $list = [
            'http://www.google.com',
            'http://www.google.com/page1',
            'http://www.google.com/page2'
        ];

        $crawlerStats = new CrawlerStats();
        foreach ($list as $item) {
            $crawlerStats->addInternalLink($item);
        }
        

        foreach ($list as $item) {
            $this->assertEquals($item, $crawlerStats->getNextInternalLinkNotFetched());
        }
    }
}
