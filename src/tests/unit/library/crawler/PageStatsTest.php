<?php

use PHPUnit\Framework\TestCase;

/**
 * PageStatsTest
 */
class PageStatsTest extends TestCase
{
    /** @test */
    public function test_create_PageStats()
    {
        $url = 'https://www.domain.com';
        $statusCode = 200;
        $title = "This is my page";
        $load = 30000;
        $wordCount = 400;


        $page = new \eduluz1976\Crawler\PageStats($url, $statusCode, $title, $load, $wordCount);

        $this->assertEquals($url, $page->getUrl());
        $this->assertEquals($load, $page->getLoad());
        $this->assertEquals($statusCode, $page->getStatusCode());
        $this->assertEquals($title, $page->getTitle());
        $this->assertEquals($wordCount, $page->getWordsCount());
    }
}
